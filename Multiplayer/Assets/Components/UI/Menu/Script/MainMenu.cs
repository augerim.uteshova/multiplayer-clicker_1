using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;


public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button _hostButton;
    [SerializeField] private Button _clientButton;
    [SerializeField] private GameObject _root;

     private void Awake()
     {
        _clientButton.onClick.AddListener(() => StartMultiplayer(true));
        _hostButton.onClick.AddListener(() => StartMultiplayer(false));
     }

     private void StartMultiplayer(bool isHost)
     {
        if(isHost)
        {
            NetworkManager.Singleton.StartHost();
        }
        else
        {
            NetworkManager.Singleton.StartClient();
        }
        _root.SetActive(false);
     }
}
