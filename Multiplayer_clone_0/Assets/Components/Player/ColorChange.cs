using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{
    [SerializeField] Material[] _material;
    [SerializeField] Renderer _renderer;

    private int _index = 1;

    private void Start()
    {
        _renderer = GetComponent<Renderer>();
        _renderer.enabled = true; 
    }

    private void OnMouseCliked()
    {
        if(_material.Length == 0)
        {
            return;
        }

        if(Input.GetMouseButtonDown(0))
        {
            _index += 1;
            if(_index == _material.Length + 1)
            {
                _index = 1;
            }
            print(_index);
            _renderer.sharedMaterial = _material [_index-1];
            
        }
    }
}
